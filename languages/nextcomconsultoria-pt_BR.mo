��    6      �  I   |      �     �  
   �     �     �     �     �     �          "     4     =     F     N  
   Z  
   e     p     x     �     �     �     �     �     �     �     �     �     �     �       	     
        )  	   .     8     =     B  
   G     R     ^     g     v     }     �     �     �     �  	   �     �     �  	   �     �     
       k       �     �     �     �     �     �     �     	     	     5	     D	  	   U	     _	     o	     }	     �	     �	     �	  #   �	     �	  	   �	     �	     �	     
     
     
     .
     ?
     R
     W
     e
     u
  
   }
     �
     �
  	   �
     �
     �
  	   �
     �
  	   �
     �
     �
     	          )  
   7     B     W  	   n     x     �     �                2   1         $      #      /   6   '                 4   "      &                *   +   %   ,      	                  -              .                  5      3                                  0       (               
   !              )          About About Text About Title Account Analysis Account Analysis Title Add Another Client Add Another Partner Add Another Solution Add Another Stage Add Logo Add icon Address Button Text Button URL Client {#} Clients Contact Contact CTA Contact Form Shortcode Contact Title Content Content Subtitle Description E-mail Footer Footer Contact Footer Text Google Maps URL Hero Hero Text Hero Title Icon Know more Logo Name News News Title Partner {#} Partners Partners Title Phones Remove Client Remove Partner Remove Solution Remove Stage Solution {#} Solutions Solutions Text Solutions Title Stage {#} Summary of content Title URL Project-Id-Version: 
PO-Revision-Date: 2019-06-11 11:19-0300
Last-Translator: 
Language-Team: 
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.6
X-Poedit-Basepath: ..
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-KeywordsList: __
X-Poedit-SearchPath-0: nextcomconsultoria.php
 Sobre Texto do Sobre Título do Sobre Análise da Conta Título da Análise da Conta Adicionar Novo Cliente Adicionar outro Parceiro Adicionar outra Solução Adicionar outra Etapa Adicionar Logo Adicionar ícone Endereço Texto do Botão URL do Botão Cliente {#} Clientes Contato Contato CTA Shortcode do Formulário de Contato Título do Contato Conteúdo Subtítulo do conteúdo Descrição E-mail Rodapé Contatos do Rodapé Texto do Rodapé URL do Google Maps Hero Texto do Hero Título do Hero Ícones Saiba mais Logo Nome Novidades Título das Novidades Parceiro {#} Parceiros Título de Parceiros Telefones Remover Cliente Remover Parceiro Remover Solução Remover Etapa Solução {#} Soluções Texto das Soluções Título das Soluções Etapa {#} Resumo do conteúdo Título URL 
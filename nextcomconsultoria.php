<?php
/*
Plugin Name: Nextcom Consultoria - mu-plugin
Plugin URI: https://nextcomconsultoria.com.br
Description: Customizations for nextcomconsultoria.com.br site (WordPress CPT and CPF)
Author: WeDo Digital
Version: 1.0.0.1
Author URI: https://wedodigital.com.br/
Text Domain: nextcomconsultoria
*/

// If this file is called directly, abort.
if (!defined('WPINC')) {
    die;
}

/**
 * Load Translation
 *
 * @return void
 */
add_action(
    'plugins_loaded', function () {
        load_muplugin_textdomain('nextcomconsultoria', basename(dirname(__FILE__)) . '/languages');
    }
);

/**
 * Hide editor from all pages
 */
add_action(
    'admin_init', function () {
        remove_post_type_support('page', 'editor');
    }
);

/*********************************************************************************** 
 * Callback Functions 
 **********************************************************************************/

 /**
 * Metabox for Page Slug
 * @author Tom Morton
 * @link https://github.com/CMB2/CMB2/wiki/Adding-your-own-show_on-filters
 *
 * @param bool $display
 * @param array $meta_box
 * @return bool display metabox
 */
function be_metabox_show_on_slug($display, $meta_box) 
{
    if (!isset($meta_box['show_on']['key'], $meta_box['show_on']['value'])) {
        return $display;
    }

    if ('slug' !== $meta_box['show_on']['key']) {
        return $display;
    }

    $post_id = 0;

    // If we're showing it based on ID, get the current ID
    if (isset($_GET['post'])) {
        $post_id = $_GET['post'];
    } elseif (isset($_POST['post_ID'])) {
        $post_id = $_POST['post_ID'];
    }

    if (!$post_id) {
        return $display;
    }

    $slug = get_post($post_id)->post_name;

    // See if there's a match
    return in_array($slug, (array) $meta_box['show_on']['value']);
}
add_filter('cmb2_show_on', 'be_metabox_show_on_slug', 10, 2);

/**
 * Gets a number of terms and displays them as options
* 
* @param CMB2_Field $field 
* 
* @return array An array of options that matches the CMB2 options array
*/
function fabservicos_getTermOptions($field) 
{
    $args = $field->args('get_terms_args');
    $args = is_array($args) ? $args : array();

    $args = wp_parse_args($args, array('taxonomy' => 'category'));

    $taxonomy = $args['taxonomy'];

    $terms = (array) cmb2_utils()->wp_at_least('4.5.0')
        ? get_terms($args)
        : get_terms($taxonomy, $args);

    // Initate an empty array
    $term_options = array();
    if (!empty($terms) ) {
        foreach ($terms as $term) {
            $term_options[ $term->term_id ] = $term->name;
        }
    }
    return $term_options;
}

/**
 *
 * Custom Post Types
 *
 */

/**************************
 * Front-page Custom Fields
 **************************/
function cmb_nextcom_frontpage()
{
    // Start with an underscore to hide fields from custom fields list
    $prefix = '_nextcomconsultoria_frontpage_';

    /******
     * Hero
     ******/
    $cmb_hero = new_cmb2_box(
        array(
            'id'            => 'frontpage_hero_id',
            'title'         => __('Hero', 'nextcomconsultoria'),
            'object_types'  => array('page'), // post type
            'show_on' => array('key' => 'slug', 'value' => 'front-page'),
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, // Show field names on the left
        )
    );

    //Hero Title
    $cmb_hero->add_field( 
        array(
            'name'       => __('Hero Title', 'nextcomconsultoria'),
            'desc'       => '',
            'id'         => $prefix . 'hero_title',
            'type'       => 'textarea_code',
        )
    );

    //Hero Text
    $cmb_hero->add_field( 
        array(
            'name'       => __('Hero Text', 'nextcomconsultoria'),
            'desc'       => '',
            'id'         => $prefix . 'hero_text',
            'type'       => 'textarea_code',
        )
    );

    //Hero Button Text
    $cmb_hero->add_field( 
        array(
            'name'       => __('Button Text', 'nextcomconsultoria'),
            'desc'       => '',
            'default'    => __('Know more', 'nextcomconsultoria'),
            'id'         => $prefix . 'hero_btn_text',
            'type'       => 'text_small',
        )
    );

    //Hero Button URL
    $cmb_hero->add_field( 
        array(
            'name'       => __('Button URL', 'nextcomconsultoria'),
            'desc'       => '',
            'id'         => $prefix . 'hero_btn_url',
            'type'       => 'text_url',
        )
    );


    /******
     * About
     ******/
    $cmb_about = new_cmb2_box(
        array(
            'id'            => 'frontpage_about_id',
            'title'         => __('About', 'nextcomconsultoria'),
            'object_types'  => array('page'), // post type
            'show_on' => array('key' => 'slug', 'value' => 'front-page'),
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, // Show field names on the left
        )
    );

    //About Title
    $cmb_about->add_field( 
        array(
            'name'       => __('About Title', 'nextcomconsultoria'),
            'desc'       => '',
            'id'         => $prefix . 'about_title',
            'type'       => 'textarea_code',
        )
    );

    //About Text
    $cmb_about->add_field( 
        array(
            'name'  => __('About Text', 'nextcomconsultoria'),
            'desc'  => '',
            'id'    => $prefix . 'about_text',
            'type'  => 'textarea_code',
        )
    );
    
    /******
     * Account Analysis
     ******/
    $cmb_analyze = new_cmb2_box(
        array(
            'id'            => 'frontpage_analyze_id',
            'title'         => __('Account Analysis', 'nextcomconsultoria'),
            'object_types'  => array('page'), // post type
            'show_on' => array('key' => 'slug', 'value' => 'front-page'),
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, // Show field names on the left
        )
    );

    //Account Analysis Title
    $cmb_analyze->add_field( 
        array(
            'name'       => __('Account Analysis Title', 'nextcomconsultoria'),
            'desc'       => '',
            'id'         => $prefix . 'analyze_title',
            'type'       => 'textarea_code',
        )
    );
    
    //Account Analysis Group
    $analysis_id = $cmb_analyze->add_field( 
        array(
            'id'          => $prefix.'analyze_analysis',
            'type'        => 'group',
            'description' => '', 
            // 'repeatable'  => false, // use false if you want non-repeatable group
            'options'     => array(
                'group_title'   => __('Stage {#}', 'nextcomconsultoria'), // since version 1.1.4, {#} gets replaced by row number
                'add_button'    => __('Add Another Stage', 'nextcomconsultoria'),
                'remove_button' => __('Remove Stage', 'nextcomconsultoria'),
                'sortable'      => true, // beta
            ),
        )
    );

    //Stage Title
    $cmb_analyze->add_group_field( 
        $analysis_id, array(
            'name' => __('Title', 'nextcomconsultoria'),
            'id'   => 'title',
            'type' => 'text',
        ) 
    );

    //Stage Description
    $cmb_analyze->add_group_field( 
        $analysis_id, array(
        'name' => __('Description', 'nextcomconsultoria'),
        'id'   => 'desc',
        'description' => '',
        'type' => 'textarea_small',
        ) 
    );

    /**********
     * Partners
     **********/
    $cmb_partner = new_cmb2_box(
        array(
            'id'            => 'frontpage_partners_id',
            'title'         => __('Partners', 'nextcomconsultoria'),
            'object_types'  => array('page'), // post type
            'show_on' => array('key' => 'slug', 'value' => 'front-page'),
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, // Show field names on the left
        )
    );
    
    //Partners Title
    $cmb_partner->add_field( 
        array(
            'name'       => __('Partners Title', 'nextcomconsultoria'),
            'desc'       => '',
            'id'         => $prefix . 'partners_title',
            'type'       => 'textarea_code',
        )
    );

    //Partner Group
    $partner_id = $cmb_partner->add_field( 
        array(
            'id'          => $prefix.'partners',
            'type'        => 'group',
            'description' => '', 
            // 'repeatable'  => false, // use false if you want non-repeatable group
            'options'     => array(
                'group_title'   => __('Partner {#}', 'nextcomconsultoria'), // since version 1.1.4, {#} gets replaced by row number
                'add_button'    => __('Add Another Partner', 'nextcomconsultoria'),
                'remove_button' => __('Remove Partner', 'nextcomconsultoria'),
                'sortable'      => true, // beta
            ),
        )
    );

    //Partner Name
    $cmb_partner->add_group_field( 
        $partner_id, array(
            'name' => __('Name', 'nextcomconsultoria'),
            'id'   => 'name',
            'type' => 'text',
        ) 
    );

    //Partner URL
    $cmb_partner->add_group_field( 
        $partner_id, array(
            'name' => __('URL', 'nextcomconsultoria'),
            'id'   => 'url',
            'type' => 'text_url',
        ) 
    );
    
    //Partner Logo
    $cmb_partner->add_group_field( 
        $partner_id, array(
            'name'        => __('Logo', 'nextcomconsultoria'),
            'description' => '',
            'id'          => 'logo',
            'type'        => 'file',
            // Optional:
            'options' => array(
                    'url' => false, // Hide the text input for the url
            ),
            'text'    => array(
                    'add_upload_file_text' => __('Add Logo', 'nextcomconsultoria'),
            ),
            // query_args are passed to wp.media's library query.
            'query_args' => array(
                    //'type' => 'image/jpeg', // Make library only display PDFs.
                    // Or only allow gif, jpg, or png images
                    'type' => array(
                    // 	'image/gif',
                        'image/jpeg',
                        'image/png',
                        'image/svg+xml',
                ),
            ),
            'preview_size' => array(180, 180)
        )
    );

    /******
     * Solutions
     ******/
    $cmb_solutions = new_cmb2_box(
        array(
            'id'            => 'frontpage_solutions_id',
            'title'         => __('Solutions', 'nextcomconsultoria'),
            'object_types'  => array('page'), // post type
            'show_on' => array('key' => 'slug', 'value' => 'front-page'),
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, // Show field names on the left
        )
    );

    //Solutions Title
    $cmb_solutions->add_field( 
        array(
            'name'       => __('Solutions Title', 'nextcomconsultoria'),
            'desc'       => '',
            'id'         => $prefix . 'solutions_title',
            'type'       => 'textarea_code',
        )
    );

    //Solutions Text
    $cmb_solutions->add_field( 
        array(
            'name'  => __('Solutions Text', 'nextcomconsultoria'),
            'desc'  => '',
            'id'    => $prefix . 'solutions_text',
            'type'  => 'textarea_code',
        )
    );
    
    //Solutions Group
    $solutions_id = $cmb_solutions->add_field( 
        array(
            'id'          => $prefix.'solutions',
            'type'        => 'group',
            'description' => '', 
            // 'repeatable'  => false, // use false if you want non-repeatable group
            'options'     => array(
                'group_title'   => __('Solution {#}', 'nextcomconsultoria'), // since version 1.1.4, {#} gets replaced by row number
                'add_button'    => __('Add Another Solution', 'nextcomconsultoria'),
                'remove_button' => __('Remove Solution', 'nextcomconsultoria'),
                'sortable'      => true, // beta
            ),
        )
    );

    //Solution Item Icon
    $cmb_solutions->add_group_field(
        $solutions_id, array(
            'name'    => __('Icon', 'nextcomconsultoria'),
            'desc'    => "",
            'id'      => 'icon',
            'type'    => 'file',
            // Optional:
            'options' => array(
                    'url' => false, // Hide the text input for the url
            ),
            'text'    => array(
                    'add_upload_file_text' => __('Add icon', 'nextcomconsultoria'), 
            ),
            // query_args are passed to wp.media's library query.
            'query_args' => array(
                    //'type' => 'image/jpeg', // Make library only display PDFs.
                    // Or only allow gif, jpg, or png images
                    'type' => array(
                    // 	'image/gif',
                            'image/jpeg',
                            'image/png',
                            'image/svg+xml',
                    ),
            ),
            'preview_size' => array(150, 150) //'large', // Image size to use when previewing in the admin.
        ) 
    );

    //Solution Title
    $cmb_solutions->add_group_field( 
        $solutions_id, array(
            'name' => __('Title', 'nextcomconsultoria'),
            'id'   => 'title',
            'type' => 'text',
        ) 
    );

    //Solution Description
    $cmb_solutions->add_group_field( 
        $solutions_id, array(
        'name' => __('Description', 'nextcomconsultoria'),
        'id'   => 'desc',
        'description' => '',
        'type' => 'textarea_small',
        ) 
    );

    /**********
     * Clients
     **********/
    $cmb_clients = new_cmb2_box(
        array(
            'id'            => 'frontpage_clients_id',
            'title'         => __('Clients', 'nextcomconsultoria'),
            'object_types'  => array('page'), // post type
            'show_on' => array('key' => 'slug', 'value' => 'front-page'),
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, // Show field names on the left
        )
    );

    //Client Group
    $clients_id = $cmb_clients->add_field( 
        array(
            'id'          => $prefix.'clients',
            'type'        => 'group',
            'description' => '', 
            // 'repeatable'  => false, // use false if you want non-repeatable group
            'options'     => array(
                'group_title'   => __('Client {#}', 'nextcomconsultoria'), // since version 1.1.4, {#} gets replaced by row number
                'add_button'    => __('Add Another Client', 'nextcomconsultoria'),
                'remove_button' => __('Remove Client', 'nextcomconsultoria'),
                'sortable'      => true, // beta
            ),
        )
    );

    //Client Name
    $cmb_clients->add_group_field( 
        $clients_id, array(
            'name' => __('Name', 'nextcomconsultoria'),
            'id'   => 'name',
            'type' => 'text',
        ) 
    );

    //Client URL
    $cmb_clients->add_group_field( 
        $clients_id, array(
            'name' => __('URL', 'nextcomconsultoria'),
            'id'   => 'url',
            'type' => 'text_url',
        ) 
    );
    
    //Client Logo
    $cmb_clients->add_group_field( 
        $clients_id, array(
            'name'        => __('Logo', 'nextcomconsultoria'),
            'description' => '',
            'id'          => 'logo',
            'type'        => 'file',
            // Optional:
            'options' => array(
                    'url' => false, // Hide the text input for the url
            ),
            'text'    => array(
                    'add_upload_file_text' => __('Add Logo', 'nextcomconsultoria'),
            ),
            // query_args are passed to wp.media's library query.
            'query_args' => array(
                    //'type' => 'image/jpeg', // Make library only display PDFs.
                    // Or only allow gif, jpg, or png images
                    'type' => array(
                    // 	'image/gif',
                        'image/jpeg',
                        'image/png',
                        'image/svg+xml',
                ),
            ),
            'preview_size' => array(180, 180)
        )
    );

    /******
     * News
     ******/
    $cmb_news = new_cmb2_box(
        array(
            'id'            => 'frontpage_news_id',
            'title'         => __('News', 'nextcomconsultoria'),
            'object_types'  => array('page'), // post type
            'show_on' => array('key' => 'slug', 'value' => 'front-page'),
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, // Show field names on the left
        )
    );

    //News Title
    $cmb_news->add_field( 
        array(
            'name'       => __('News Title', 'nextcomconsultoria'),
            'desc'       => '',
            'id'         => $prefix . 'news_title',
            'type'       => 'textarea_code',
        )
    );

    /******
     * Contact
     ******/
    $cmb_contact = new_cmb2_box(
        array(
            'id'            => 'frontpage_contact_id',
            'title'         => __('Contact', 'nextcomconsultoria'),
            'object_types'  => array('page'), // post type
            'show_on' => array('key' => 'slug', 'value' => 'front-page'),
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, // Show field names on the left
        )
    );

    //Contact Title
    $cmb_contact->add_field( 
        array(
            'name'       => __('Contact Title', 'nextcomconsultoria'),
            'desc'       => '',
            'id'         => $prefix . 'contact_title',
            'type'       => 'textarea_code',
        )
    );

    //Contact CTA
    $cmb_contact->add_field( 
        array(
            'name'       => __('Contact CTA', 'nextcomconsultoria'),
            'desc'       => '',
            'id'         => $prefix . 'contact_text',
            'type'       => 'textarea_code',
        )
    );

    //Contact Form Shortcode
    $cmb_contact->add_field( 
        array(
            'name'       => __('Contact Form Shortcode', 'nextcomconsultoria'),
            'desc'       => '',
            'id'         => $prefix . 'contact_form_shortcode',
            'type'       => 'text',
        )
    );

    //Contact Map URL
    $cmb_contact->add_field( 
        array(
            'name'       => __('Google Maps URL', 'nextcomconsultoria'),
            'desc'       => '',
            'id'         => $prefix . 'contact_map_url',
            'type'       => 'textarea_code',
        )
    );
    
    /******
     * Footer
     ******/
    $cmb_footer = new_cmb2_box(
        array(
            'id'            => 'frontpage_footer_id',
            'title'         => __('Footer', 'nextcomconsultoria'),
            'object_types'  => array('page'), // post type
            'show_on' => array('key' => 'slug', 'value' => 'front-page'),
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, // Show field names on the left
        )
    );

    //Footer Text
    $cmb_footer->add_field( 
        array(
            'name'       => __('Footer Text', 'nextcomconsultoria'),
            'desc'       => '',
            'id'         => $prefix . 'footer_text',
            'type'       => 'textarea_code',
        )
    );

    //Footer Contact
    $cmb_footer->add_field( 
        array(
            'name'       => __('Footer Contact', 'nextcomconsultoria'),
            'desc'       => '',
            'id'         => $prefix . 'footer_contact',
            'type'       => 'title',
        )
    );

    //Footer Address
    $cmb_footer->add_field( 
        array(
            'name'       => __('Address', 'nextcomconsultoria'),
            'desc'       => '',
            'id'         => $prefix . 'footer_address',
            'type'       => 'textarea_code',
        )
    );

    //Footer Phone
    $cmb_footer->add_field( 
        array(
            'name'       => __('Phones', 'nextcomconsultoria'),
            'desc'       => '',
            'id'         => $prefix . 'footer_phones',
            'type'       => 'textarea_code',
        )
    );

    //Footer E-mail
    $cmb_footer->add_field( 
        array(
            'name'       => __('E-mail', 'nextcomconsultoria'),
            'desc'       => '',
            'id'         => $prefix . 'footer_email',
            'type'       => 'text',
        )
    );
}
add_action('cmb2_admin_init', 'cmb_nextcom_frontpage');

/******************************
 * Home (Blog)
 ******************************/
function cmb_nextcom_blog()
{
    // Start with an underscore to hide fields from custom fields list
    $prefix = '_nextcomconsultoria_blog_';

    /******
     * Hero
     ******/
    $cmb_hero = new_cmb2_box(
        array(
            'id'            => 'blog_hero_id',
            'title'         => __('Hero', 'nextcomconsultoria'),
            'object_types'  => array('page'), // post type
            'show_on'       =>  array( 'key' => 'slug', 'value' => 'blog'),
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, // Show field names on the left
        )
    );

    //Hero Title
    $cmb_hero->add_field( 
        array(
            'name'       => __('Hero Title', 'nextcomconsultoria'),
            'desc'       => '',
            'id'         => $prefix . 'hero_title',
            'type'       => 'text',
        )
    );

}
add_action('cmb2_admin_init', 'cmb_nextcom_blog');

/**
 * Single Post (Blog)
 */
add_action(
    'cmb2_admin_init', function () {
        // Start with an underscore to hide fields from custom fields list
        $prefix = '_nextcomconsultoria_post';

        /******
         * Hero
         ******/
        $cmb_hero = new_cmb2_box(
            array(
                'id'            => 'post_hero_id',
                'title'         => __('Hero', 'nextcomconsultoria'),
                'object_types'  => array('post'), // post type
                //'show_on'       =>  array('key' => 'page-template', 'value' => 'views/home.blade.php'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Hero Title
        $cmb_hero->add_field( 
            array(
                'name'       => __('Hero Title', 'nextcomconsultoria'),
                'desc'       => '',
                'id'         => $prefix . 'hero_title',
                'type'       => 'text',
                'default'    => 'Blog',
            )
        );

        /**
         * Content
         */
        $cmb_content = new_cmb2_box(
            array(
                'id'            => 'post_content_id',
                'title'         => __('Content', 'nextcomconsultoria'),
                'object_types'  => array('post'), // post type
                //'show_on' => array('key' => 'slug', 'value' => 'clinica'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Content Subtitle
        $cmb_content->add_field( 
            array(
                'name'       => __('Content Subtitle', 'nextcomconsultoria'),
                'desc'       => '',
                'id'         => $prefix . 'content_subtitle',
                'type'       => 'text',
            )
        );

        //Content Subtitle
        $cmb_content->add_field( 
            array(
                'name'       => __('Summary of content', 'nextcomconsultoria'),
                'desc'       => '',
                'id'         => $prefix . 'content_summary',
                'type'       => 'textarea_code',
            )
        );
    }
);
?>
